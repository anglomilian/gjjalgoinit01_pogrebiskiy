package main.java.com.getjavajob.training.util;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by anglomilian on 04/04/2017.
 */
public class InputData {

    private static final Scanner SC = new Scanner(System.in);

    public static int enterIntNumber(String requestLine) {
        System.out.print(requestLine);
        while (!SC.hasNextInt()) {
            System.out.print(requestLine);
            SC.next();
        }
        return SC.nextInt();
    }

    public static double enterDoubleNumber(String requestLine) {
        System.out.print(requestLine);
        while (!SC.hasNextDouble()) {
            System.out.print(requestLine);
            SC.next();
        }
        return SC.nextDouble();
    }

    public static String enterString(String requestLine) {
        String outText;
        do {
            System.out.print(requestLine);
            outText = SC.nextLine();
        } while (outText.isEmpty());
        return outText;
    }

    public static int[] fillOneDimArrayRandom(int arraySize, int diffrerence) {
        int[] result = new int[arraySize];
        Random rand = new Random();
        for (int i = 0; i < arraySize; i++) {
            result[i] = rand.nextInt(diffrerence) + 1;
        }
        return result;
    }

    public static int[][] fillTwoDimArrayRandom(int sizeN, int sizeM, int difference) {
        Random rand = new Random();
        int[][] result = new int[sizeN][sizeM];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = rand.nextInt(difference) + 25;
            }
        }
        return result;
    }

    public static void printOneDimArray(String header, int[] arr) {
        System.out.print(header);
        for (int anArr : arr) {
            System.out.printf("%5d", anArr);
        }
        System.out.println();
    }

    public static void printTwoDimArray(String header, int[][] n) {
        System.out.println(header);
        for (int[] aN : n) {
            System.out.println();
            for (int j = 0; j < n[0].length; j++) {
                System.out.printf("%5d ", aN[j]);
            }
        }
        System.out.println();
    }
}
