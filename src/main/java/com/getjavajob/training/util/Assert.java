package main.java.com.getjavajob.training.util;

import java.util.Arrays;

/**
 * Created by @author anglomilian on 2/23/2017.
 */
public class Assert {
    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + "failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testNumber, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testNumber, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testNumber, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testNumber, double[] expected, double[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testNumber, int[] expected, int[] actual) {
        if (Arrays.equals(actual, expected)) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testNumber, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(actual, expected)) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + Arrays.deepToString(expected) + ", actual " + Arrays.deepToString(actual));
        }
    }

    public static void assertEquals(String testNumber, Object expected, Object actual) {
        if (expected.equals(actual)) {
            System.out.println(testNumber + " passed");
        } else {
            System.out.println(testNumber + " failed: expected " + expected + ", actual " + actual);
        }
    }
}