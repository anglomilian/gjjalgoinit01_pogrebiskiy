package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N036.getTrafficLightColor;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testGetTrafficLightColor();
    }

    private static void testGetTrafficLightColor() {
        assertEquals("TaskCh04N036Test.testGetTrafficLightColor", "red", getTrafficLightColor(3));
        assertEquals("TaskCh04N036Test.testGetTrafficLightColor", "green", getTrafficLightColor(5));
    }
}
