package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

/**
 * Created by @author anglomilian on 2/18/2017.
 */
public class TaskCh01N003 {

    public static void main(String[] args) {
        int n;
        do {
            n = enterIntNumber("Enter an integer number: ");
        } while (n <= 0);
        System.out.println("You have entered the number: " + n);
    }
}
