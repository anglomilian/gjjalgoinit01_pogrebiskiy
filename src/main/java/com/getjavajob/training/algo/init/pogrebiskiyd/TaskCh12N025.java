package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printTwoDimArray;

public class TaskCh12N025 {

    public static void main(String[] args) {
        arrA();
        arrB();
        arrV();
        arrG();
        arrD();
        arrE();
        arrZH();
        arrZ();
        arrI();
        arrK();
        arrL();
        arrM();
        arrN();
        arrO();
        arrP();
        arrR();
    }

    public static void arrA() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[0].length; j++) {
                n[i][j] = z++;
            }
        }
        printTwoDimArray("A", n);
    }

    public static void arrB() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n[0].length; i++) {
            for (int j = 0; j < n.length; j++) {
                n[j][i] = z++;
            }
        }
        printTwoDimArray("B", n);
    }

    private static void arrV() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n.length; i++) {
            for (int j = n[0].length - 1; j >= 0; j--) {
                n[i][j] = z++;
            }
        }
        printTwoDimArray("V", n);
    }

    private static void arrG() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n[0].length; i++) {
            for (int j = n.length - 1; j >= 0; j--) {
                n[j][i] = z++;
            }
        }
        printTwoDimArray("G", n);
    }

    private static void arrD() {
        int z = 1;
        int[][] n = new int[10][12];
        for (int i = 0; i < n.length; i++) {
            if (i % 2 != 0) {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = z++;
                }
            } else {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = z++;
                }
            }
        }
        printTwoDimArray("D", n);
    }

    private static void arrE() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n[0].length; i++) {
            if (i % 2 != 0) {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = z++;
                }
            } else {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = z++;
                }
            }
        }
        printTwoDimArray("E", n);
    }

    private static void arrZH() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n.length - 1; i >= 0; i--) {
            for (int j = 0; j < n[0].length; j++) {
                n[i][j] = z++;
            }
        }
        printTwoDimArray("ZH", n);
    }

    private static void arrZ() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n[0].length - 1; i >= 0; i--) {
            for (int j = 0; j < n.length; j++) {
                n[j][i] = z++;
            }
        }
        printTwoDimArray("Z", n);
    }

    private static void arrI() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n.length - 1; i >= 0; i--) {
            for (int j = n[0].length - 1; j >= 0; j--) {
                n[i][j] = z++;
            }
        }
        printTwoDimArray("I", n);
    }

    private static void arrK() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n[0].length - 1; i >= 0; i--) {
            for (int j = n.length - 1; j >= 0; j--) {
                n[j][i] = z++;
            }
        }
        printTwoDimArray("K", n);
    }

    private static void arrL() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = z++;
                }
            } else {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = z++;
                }
            }
        }
        printTwoDimArray("L", n);
    }

    private static void arrM() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n.length; i++) {
            if (i % 2 == 0) {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = z++;
                }
            } else {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = z++;
                }
            }
        }
        printTwoDimArray("M", n);
    }

    private static void arrN() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n[0].length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = z++;
                }
            } else {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = z++;
                }
            }
        }
        printTwoDimArray("N", n);
    }

    private static void arrO() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = 0; i < n[0].length; i++) {
            if (i % 2 == 0) {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = z++;
                }
            } else {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = z++;
                }
            }
        }
        printTwoDimArray("O", n);
    }

    private static void arrP() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = z++;
                }
            } else {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = z++;
                }
            }
        }
        printTwoDimArray("P", n);
    }

    private static void arrR() {
        int z = 1;
        int[][] n = new int[12][10];
        for (int i = n[0].length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = z++;
                }
            } else {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = z++;
                }
            }
        }
        printTwoDimArray("R", n);
    }
}