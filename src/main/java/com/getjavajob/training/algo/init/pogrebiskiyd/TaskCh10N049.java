package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.*;

public class TaskCh10N049 {

    public static void main(String[] args) {
        int arraySize;
        do {
            arraySize = enterIntNumber("Enter array size: ");
        } while (arraySize < 1);
        int[] forSort = fillOneDimArrayRandom(arraySize, 50);
        printOneDimArray("Array: ", forSort);
        System.out.println("Max element index is: " + (getIndexOfMaxElement(forSort, forSort.length - 1) + 1));
    }

    public static int getIndexOfMaxElement(int[] a, int i) {
        if (i == 0) {
            return 0;
        }
        int t = getIndexOfMaxElement(a, i - 1);
        return a[i] < a[t] ? t : i;
    }
}