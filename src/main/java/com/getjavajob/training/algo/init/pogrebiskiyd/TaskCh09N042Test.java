package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N042.getReverseWord;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N042Test {

    public static void main(String[] args) {
        testGetReverseWord();
    }

    private static void testGetReverseWord() {
        assertEquals("TaskCh09N042Test.testGetReverseWord", "aixino", getReverseWord("onixia"));
    }
}
