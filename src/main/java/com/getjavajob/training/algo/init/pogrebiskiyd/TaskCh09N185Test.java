package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 28/02/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N185.*;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh09N185Test {

    private static final int[] TEST1 = {4, 5, 12};
    private static final int[] TEST2 = {5, 4, 13};
    private static final int[] TEST3 = {4, 4, 12};

    public static void main(String[] args) {
        testCheckParentheses();
        testPrintAnswerA();
        testPrintAnswerB();
    }

    private static void testCheckParentheses() {
        assertEquals("TaskCh09N185Test.testCheckParentheses", TEST1, checkParentheses("a+b*2*((f/t)*(2+b*a)-pi*(4+a*b)))"));
        assertEquals("TaskCh09N185Test.testCheckParentheses", TEST2, checkParentheses("a+b*2*(((f/t)*(2+b*a)-pi*(4+a*b))"));
        assertEquals("TaskCh09N185Test.testCheckParentheses", TEST3, checkParentheses("a+b*2*((f/t)*(2+b*a)-pi*(4+a*b))"));
        System.out.println();
    }

    private static void testPrintAnswerA() {
        assertEquals("TaskCh09N185Test.testPrintAnswerA", "no", printAnswerA(TEST1));
        assertEquals("TaskCh09N185Test.testPrintAnswerA", "no", printAnswerA(TEST2));
        assertEquals("TaskCh09N185Test.testPrintAnswerA", "yes", printAnswerA(TEST3));
        System.out.println();
    }

    private static void testPrintAnswerB() {
        assertEquals("TaskCh09N185Test.testPrintAnswerB",
                "Here is excess right (close) parenthesis in your expression, pos:12", printAnswerB(TEST1));
        assertEquals("TaskCh09N185Test.testPrintAnswerB",
                "Here is excess left (open) parenthesis in your expression, total:1", printAnswerB(TEST2));
        assertEquals("TaskCh09N185Test.testPrintAnswerB", "Correct expression", printAnswerB(TEST3));
    }
}