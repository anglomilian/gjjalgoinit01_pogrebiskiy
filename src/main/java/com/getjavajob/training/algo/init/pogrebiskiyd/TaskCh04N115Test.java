package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 27/02/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N115.getChinaYear;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh04N115Test {

    public static void main(String[] args) {
        testGetChinaYear();
    }

    private static void testGetChinaYear() {
        assertEquals("TaskCh04N115Test.testGetChinaYear", "White Monkey", getChinaYear(1980));
        assertEquals("TaskCh04N115Test.testGetChinaYear", "Black Dragon", getChinaYear(1952));
        assertEquals("TaskCh04N115Test.testGetChinaYear", "Red Monkey", getChinaYear(1956));
    }
}
