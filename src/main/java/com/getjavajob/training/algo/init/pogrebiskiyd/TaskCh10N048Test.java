package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 01/03/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N048.getMinMemberOfArray;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh10N048Test {

    public static void main(String[] args) {
        testgetMinMemberOfArray();
    }

    private static void testgetMinMemberOfArray() {
        int[] testArr = {6, 50, 43, 31, 48, 12, 9, 11, 2, 36, 14, 37, 19, 9, 39, 15, 17, 33, 12, 4, 21, 47, 25, 6, 39, 29, 43, 23, 39, 34};
        assertEquals("TaskCh10N048Test.testgetMinMemberOfArray", 2, getMinMemberOfArray(testArr, testArr.length - 1));
    }
}
