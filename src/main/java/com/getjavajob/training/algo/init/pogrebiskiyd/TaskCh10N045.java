package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N045 {

    public static void main(String[] args) {
        int firstMember = enterIntNumber("Enter first member of arithmetic progression: ");
        int progressionDiff = enterIntNumber("Enter difference of arithmetic progression: ");
        int quantity = enterIntNumber("Enter quantity of the members: ");
        System.out.println(nMemberOfArithmeticProgression(firstMember, progressionDiff, quantity));
        System.out.println(sumNNumbersOfArithmeticProgression(firstMember, progressionDiff, quantity));
    }

    public static int nMemberOfArithmeticProgression(int firstMember, int progressionDiff, int quantity) {
        if (quantity == 1) {
            return firstMember;
        }
        return progressionDiff + nMemberOfArithmeticProgression(firstMember, progressionDiff, quantity - 1);
    }

    public static int sumNNumbersOfArithmeticProgression(int firstMember, int progressionDiff, int quantity) {
        if (quantity == 0) {
            return 0;
        }
        return firstMember + sumNNumbersOfArithmeticProgression(firstMember + progressionDiff, progressionDiff, quantity - 1);
    }
}