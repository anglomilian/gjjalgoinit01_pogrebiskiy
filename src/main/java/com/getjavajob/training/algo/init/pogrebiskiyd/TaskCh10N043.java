package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N043 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter number > 1 :");
        } while (number < 1);
        System.out.println("Sum of the digits of the natural number " + number + " is: " + sumOfDigits(number));
        System.out.println("Number of the digits of natural the number " + number + " is: " + numberOfDigits(number));
    }

    public static int sumOfDigits(int number) {
        return number < 10 ? number : number % 10 + sumOfDigits(number / 10);
    }

    public static int numberOfDigits(int number) {
        return number < 10 ? 1 : 1 + numberOfDigits(number / 10);
    }
}
