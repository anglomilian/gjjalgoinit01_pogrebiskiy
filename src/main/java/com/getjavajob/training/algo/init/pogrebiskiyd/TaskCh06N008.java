package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static java.lang.Math.sqrt;
import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;
import static main.java.com.getjavajob.training.util.InputData.printOneDimArray;

public class TaskCh06N008 {

    public static void main(String[] args) {
        int n;
        do {
            n = enterIntNumber("Enter n >=1 : ");
        } while (n < 1);
        printOneDimArray("Sqare sequence before " + n + " number: ", getSquareRow(n));
    }

    public static int[] getSquareRow(int n) {
        int membersQuanity = (int) sqrt(n);
        int squareRow[] = new int[membersQuanity + 1];
        for (int i = 0; i <= membersQuanity; i++) {
            squareRow[i] = i * i;
        }
        return squareRow;
    }
}