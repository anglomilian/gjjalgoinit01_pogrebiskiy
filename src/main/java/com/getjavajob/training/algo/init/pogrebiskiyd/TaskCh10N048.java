package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.*;

public class TaskCh10N048 {

    public static void main(String[] args) {
        int arraySize;
        do {
            arraySize = enterIntNumber("Enter array size:");
        } while (arraySize < 1);
        int[] forSort = fillOneDimArrayRandom(arraySize, 50);
        printOneDimArray("Array: ", forSort);
        System.out.println("Minimum array member: " + getMinMemberOfArray(forSort, forSort.length - 1));
    }

    public static int getMinMemberOfArray(int[] a, int i) {
        if (i == 0) {
            return a[0];
        }
        return a[i] < getMinMemberOfArray(a, i - 1) ? a[i] : getMinMemberOfArray(a, i - 1);
    }
}