package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N015.getWordChar;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N015Test {

    public static void main(String[] args) {
        testGetWordChar();
    }

    private static void testGetWordChar() {
        assertEquals("TaskCh09N015Test.testGetWordChar", 'x', getWordChar("alextraza", 4));
        assertEquals("TaskCh09N015Test.testGetWordChar", 'r', getWordChar("ragnaros", 6));
    }
}
