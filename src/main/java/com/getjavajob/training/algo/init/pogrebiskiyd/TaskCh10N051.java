package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

public class TaskCh10N051 {

    public static void main(String[] args) {
        int n = 5;
        first(n);
        second(n);
        third(n);
    }

    public static void first(int n) {
        if (n > 0) {
            System.out.println("first " + n);
            first(n - 1);
        }
    }

    public static void second(int n) {
        if (n > 0) {
            second(n - 1);
            System.out.println("second " + n);
        }
    }

    public static void third(int n) {
        if (n > 0) {
            System.out.println("third " + n);
            third(n - 1);
            System.out.println("third " + n);
        }
    }
}
