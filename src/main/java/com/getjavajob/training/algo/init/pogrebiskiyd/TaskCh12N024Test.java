package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N024.nDiagonal;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N024.sumConers;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 06/03/17.
 */

public class TaskCh12N024Test {

    public static void main(String[] args) {
        testSumConers();
        testNDiagonal();
    }

    private static void testSumConers() {
        int[][] testA = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testSumConers", testA, sumConers(6));
    }

    private static void testNDiagonal() {
        int[][] testB = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.testNDiagonal", testB, nDiagonal(6));
    }
}
