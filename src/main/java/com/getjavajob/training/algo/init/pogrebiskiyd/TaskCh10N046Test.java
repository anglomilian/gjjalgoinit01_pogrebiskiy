package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 01/03/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N046.nMemberOfGeometricProgression;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N046.sumNNumbersOfGeometicProgression;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh10N046Test {

    public static void main(String[] args) {
        testNMemberOfGeometicProgression();
        testSumNNumbersOfGeometicProgression();
    }

    private static void testNMemberOfGeometicProgression() {
        assertEquals("TaskCh10N046Test.testNMemberOfGeometicProgression", 512, nMemberOfGeometricProgression(1, 2, 10));
        assertEquals("TaskCh10N046Test.testNMemberOfGeometicProgression", 705032704, nMemberOfGeometricProgression(5, 10, 10));
        System.out.println();
    }

    private static void testSumNNumbersOfGeometicProgression() {
        assertEquals("TaskCh10N046Test.testSumNNumbersOfGeometicProgression", 1023, sumNNumbersOfGeometicProgression(1, 2, 10));
        assertEquals("TaskCh10N046Test.testSumNNumbersOfGeometicProgression", 1260588259, sumNNumbersOfGeometicProgression(5, 10, 10));
    }
}