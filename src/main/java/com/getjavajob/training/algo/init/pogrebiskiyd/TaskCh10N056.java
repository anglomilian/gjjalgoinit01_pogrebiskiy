package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N056 {

    public static void main(String[] args) {
        int n;
        do {
            n = enterIntNumber("Enter number >= 2: ");
        } while (n < 2);
        System.out.print("Is " + n + " a simple number? " + isSimpleNumber(n, 2));
    }

    public static boolean isSimpleNumber(int n, int p) {
        return p > n / 2 || n % p != 0 && isSimpleNumber(n, p + 1);
    }
}
