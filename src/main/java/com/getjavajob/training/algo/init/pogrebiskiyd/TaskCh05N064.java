package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

public class TaskCh05N064 {

    public static void main(String[] args) {
        int[][] districts = {{120, 20}, {200, 40}, {800, 110}, {750, 30}, {900, 20}, {400, 50},
                {500, 80}, {400, 30}, {300, 100}, {1000, 70}, {600, 60}, {550, 30}};
        System.out.printf("Density of 12 districts: %4.4f %s", calculateDensity(districts), "kilohumans/km2");
    }

    public static double calculateDensity(int[][] dis) {
        double s = 0;
        double n = 0;
        for (int i = 0; i < 12; i++) {
            n += dis[i][0];
            s += dis[i][1];
        }
        return n / s;
    }
}