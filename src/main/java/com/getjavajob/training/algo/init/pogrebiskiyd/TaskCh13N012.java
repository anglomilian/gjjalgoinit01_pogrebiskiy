package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by @author anglomilian on 3/11/2017.
 */

import java.util.ArrayList;
import java.util.List;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.Database.*;

public class TaskCh13N012 {

    public static void main(String[] args) {
        Employee employee = new Employee("Denis", "Vasilyevich", "Pogrebiskiy", 3, 2011);
        System.out.println("Worked for " + employee.getWorkedYears(3, 2017) + " years");
        Database.fillDatabase();
        System.out.println("Substring search result:");
        List<Employee> subStringEmployees = searchBySubstring("kar");
        printEmployees(subStringEmployees);
        System.out.println("Worked years result:");
        List<Employee> workedForYearsEmployees = getEmployeesWorkedForYears(5, 3, 2017);
        printEmployees(workedForYearsEmployees);
    }
}

class Employee {

    private String firstName;
    private String middleName;
    private String lastName;
    private String address;
    private int inMonth;
    private int inYear;

    public Employee(String firstName, String middleName, String lastName, String address, int inMonth, int inYear) {
        setFirstName(firstName);
        setMiddleName(middleName);
        setLastName(lastName);
        setAddress(address);
        setInMonth(inMonth);
        setInYear(inYear);
    }

    //no middleName
    public Employee(String firstName, String lastName, String address, int inMonth, int inYear) {
        this(firstName, lastName, "", address, inMonth, inYear);
    }

    public int getWorkedYears(int month, int year) {
        int result = year - inYear;
        return month >= inMonth ? result : result - 1;
    }

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof Employee) {
            Employee compareEmployee = (Employee) object;
            if (this.firstName.equals(compareEmployee.firstName)
                    && this.lastName.equals(compareEmployee.lastName)
                    && this.middleName.equals(compareEmployee.middleName)
                    && this.address.equals(compareEmployee.address)
                    && this.inMonth == compareEmployee.inMonth
                    && this.inYear == compareEmployee.inYear) {
                return true;
            }
        }
        return false;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getInMonth() {
        return inMonth;
    }

    public void setInMonth(int inMonth) {
        this.inMonth = inMonth;
    }

    public int getInYear() {
        return inYear;
    }

    public void setInYear(int inYear) {
        this.inYear = inYear;
    }
}

class Database {

    private static List<Employee> employees = new ArrayList<>();

    public static void fillDatabase() {
        employees.add(new Employee("Andranik", "Karenovich", "Hadjabekyan", "Frolova,45-8", 3, 2015));
        employees.add(new Employee("Maxim", "Petrovich", "Orlov", "Neglinnaya,25-56", 5, 2016));
        employees.add(new Employee("Peter", "Vasilyevich", "Molokov", "Figurnaya,78-23", 1, 2015));
        employees.add(new Employee("Gleb", "Markovich", "Ikonnikov", "Rostova,67-91", 6, 2012));
        employees.add(new Employee("Rodion", "Romanovich", "Raskolnikov", "Grazhdanskaya,19-13", 2, 2016));
        employees.add(new Employee("Dmitriy", "Fedorovich", "Karamazov", "Voskhodnaya,56-67", 4, 2016));
        employees.add(new Employee("Ivan", "Fedorovich", "Karamazov", "Neglinnaya,34-90", 2, 2014));
        employees.add(new Employee("Alexey", "Fedorovich", "Karamazov", "Prazhskaya,45-71", 10, 2015));
        employees.add(new Employee("Pavel", "Fedorovich", "Smerdyakov", "Moskovskaya,31-74", 11, 2016));
        employees.add(new Employee("Asokha", "Jedi", "Tano", "Akhmadulina,60", 9, 2012));
        employees.add(new Employee("Klim", "Ivanovich", "Samgin", "Tverskaya,36-12", 9, 2014));
        employees.add(new Employee("Rryuk", "DeathGod", "Shinigami", "Seligerskaya,67-20", 2, 2014));
        employees.add(new Employee("Illidan", "Storm", "Rage", "Black Temple", 10, 1999));
        employees.add(new Employee("Tyrande", "Wisper", "Wind", "Darnassus", 5, 2011));
        employees.add(new Employee("Katrana", "Onixia", "Prestor", "StormWind", 6, 2016));
        employees.add(new Employee("Sylvana", "Wind", "Runner", "UnderCity", 1, 2014));
        employees.add(new Employee("Totoro", "HairBall", "CatBear", "Lesnaya,34-34", 5, 2016));
        employees.add(new Employee("Mononoke", "Princess", "Polyanka,78", 6, 2015)); //no middleName
        employees.add(new Employee("Chun", "Li", "Kievskaya,43-8", 8, 2016)); //no middleName
    }

    public static List<Employee> searchBySubstring(String substring) {
        List<Employee> searchEmployees = new ArrayList<>();
        for (Employee list : employees) {
            String fullName = list.getFirstName() + " " + list.getLastName() + " " + list.getMiddleName();
            if (fullName.toLowerCase().contains(substring.toLowerCase())) {
                searchEmployees.add(list);
            }
        }
        return searchEmployees;
    }

    public static List<Employee> getEmployeesWorkedForYears(int years, int sinceMonth, int sinceYear) {
        List<Employee> workedEmployees = new ArrayList<>();
        for (Employee list : employees) {
            if (list.getWorkedYears(sinceMonth, sinceYear) >= years) {
                workedEmployees.add(list);
            }
        }
        return workedEmployees;
    }

    public static void printEmployees(List<Employee> employees) {
        for (Employee list : employees) {
            String allInOne = list.getFirstName() + " " + list.getMiddleName() + " " + list.getLastName() + ", address: " +
                    list.getAddress() + ", employment date: " + list.getInMonth() + "." + list.getInYear();
            System.out.println(allInOne);
        }
    }
}