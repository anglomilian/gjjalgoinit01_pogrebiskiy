package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printTwoDimArray;

public class TaskCh12N023 {

    public static void main(String[] args) {
        int arrSize = 7;
        int[][] filled = fillArrA(arrSize);
        printTwoDimArray("A", filled);
        filled = fillArrB(arrSize);
        printTwoDimArray("B", filled);
        filled = fillArrC(arrSize);
        printTwoDimArray("C", filled);
    }

    public static int[][] fillArrA(int arrSize) {
        int[][] feelA = new int[arrSize][arrSize];
        int actualSize = feelA.length - 1;
        int j = actualSize;
        for (int i = 0; i < feelA.length; i++) {
            feelA[i][j - i] = 1;
            for (j = actualSize; j >= 0; j--) {
                if (i == j) {
                    feelA[i][j] = 1;
                }
            }
            j = actualSize;
        }
        return feelA;
    }

    public static int[][] fillArrB(int arrSize) {
        int[][] feelB = new int[arrSize][arrSize];
        int actualSize = feelB.length - 1;
        int j = actualSize;
        for (int i = 0; i < feelB.length; i++) {
            feelB[i][j - i] = 1;
            feelB[i][feelB.length / 2] = 1;
            for (j = actualSize; j >= 0; j--) {
                if (i == j) {
                    feelB[i][j] = 1;
                }
                if (i == feelB.length / 2) {
                    feelB[i][j] = 1;
                }
            }
            j = actualSize;
        }
        return feelB;
    }

    public static int[][] fillArrC(int arrSize) {
        int[][] feelC = new int[arrSize][arrSize];
        for (int i = 0; i <= feelC.length / 2; i++) {
            for (int j = i; j < feelC.length - i; j++) {
                feelC[i][j] = 1;
                feelC[feelC.length - i - 1][j] = 1;
            }
        }
        return feelC;
    }
}