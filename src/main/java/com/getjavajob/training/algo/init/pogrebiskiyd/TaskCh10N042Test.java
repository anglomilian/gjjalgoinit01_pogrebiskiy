package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 28/02/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N042.myPow;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh10N042Test {

    public static void main(String[] args) {
        testMyPow();
    }

    private static void testMyPow() {
        assertEquals("TaskCh10N042Test.testNFactorial", 256, myPow(2, 8));
        assertEquals("TaskCh10N042Test.testNFactorial", 100000, myPow(10, 5));
    }
}
