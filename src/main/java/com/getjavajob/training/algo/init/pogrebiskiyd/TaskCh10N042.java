package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterDoubleNumber;
import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N042 {

    public static void main(String[] args) {
        double x;
        do {
            x = enterDoubleNumber("Enter number: ");
        } while (x < 1);
        int y;
        do {
            y = enterIntNumber("Enter power: ");
        } while (y < 0);
        System.out.print(x + " to the power of " + y + " : " + myPow(x, y));
    }

    public static double myPow(double x, int y) {
        if (y == 0) {
            return 1;
        } else if (y == 1) {
            return x;
        }
        return x * myPow(x, y - 1);
    }
}