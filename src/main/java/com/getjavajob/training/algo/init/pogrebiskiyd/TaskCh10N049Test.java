package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N049.getIndexOfMaxElement;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N049Test {

    public static void main(String[] args) {
        int[] testArr = {6, 500, 43, 31, 48, 12, 9, 11, 2, 36, 14, 37, 19, 9, 39, 15, 17, 33, 12, 4, 21, 47, 25, 6, 39, 29, 43, 23, 39, 34};
        assertEquals("TaskCh10N049Test.testgetMinMemberOfArray", 2, getIndexOfMaxElement(testArr, testArr.length - 1) + 1);
    }
}
