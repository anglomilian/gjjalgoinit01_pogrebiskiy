package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh02N043.checkDividingAb;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 2/26/2017.
 */

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testCheckDividingAB();
    }

    private static void testCheckDividingAB() {
        assertEquals("TaskCh02N039Test.testCheckDividingAB", 1, checkDividingAb(20, 10));
        assertEquals("TaskCh02N039Test.testCheckDividingAB", 1, checkDividingAb(40, 80));
        assertEquals("TaskCh02N039Test.testCheckDividingAB", 232, checkDividingAb(40, 33));
    }
}
