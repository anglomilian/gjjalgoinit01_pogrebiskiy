package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N041.nFactorial;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh10N041Test {

    public static void main(String[] args) {
        testNFactorial();
    }

    private static void testNFactorial() {
        assertEquals("TaskCh10N041Test.testNFactorial", 720, nFactorial(6));
        assertEquals("TaskCh10N041Test.testNFactorial", 2004310016, nFactorial(15));
    }
}
