package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;
import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N015 {

    public static void main(String[] args) {
        String oneWord;
        do {
            oneWord = enterString("Enter one word: ");
        } while (oneWord.contains(" "));
        int charNumber;
        do {
            charNumber = enterIntNumber("Enter character number: ");
        } while (charNumber < 1 || charNumber > oneWord.length());
        System.out.println(getWordChar(oneWord, charNumber));
    }

    public static char getWordChar(String oneWord, int charNumber) {
        return oneWord.charAt(charNumber - 1);
    }
}