package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N234.delArRow;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N234.delArString;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 3/8/2017.
 */

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDelArRow();
        testDelArString();
    }

    private static void testDelArRow() {
        int[][] testDelRow = new int[][]{
                {14, 21, 30, 14, 27},
                {9, 38, 35, 17, 27},
                {40, 11, 14, 40, 13},
                {3, 22, 3, 22, 48},
        };
        int[][] testDeletedRow = new int[][]{
                {14, 21, 14, 27, 0},
                {9, 38, 17, 27, 0},
                {40, 11, 40, 13, 0},
                {3, 22, 22, 48, 0},
        };
        assertEquals("TaskCh12N234Test.testDelArRow", testDeletedRow, delArRow(testDelRow, 2));
    }

    private static void testDelArString() {
        int[][] testDelString = new int[][]{
                {16, 42, 31, 4, 27},
                {8, 8, 49, 15, 13},
                {17, 30, 6, 21, 11},
                {49, 42, 32, 50, 35},

        };
        int[][] testDeletedString = new int[][]{
                {16, 42, 31, 4, 27},
                {8, 8, 49, 15, 13},
                {49, 42, 32, 50, 35},
                {0, 0, 0, 0, 0},
        };
        assertEquals("TaskCh12N234Test.testDelArString", testDeletedString, delArString(testDelString, 2));
    }
}
