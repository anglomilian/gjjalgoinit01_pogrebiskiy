package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh02N039 {

    public static void main(String[] args) {
        int hours;
        do {
            hours = enterIntNumber("Enter 0 <= hours <= 23: ");
        } while (hours < 0 || hours > 23);
        int minutes;
        do {
            minutes = enterIntNumber("Enter 0 <= minutes <= 59: ");
        } while (minutes < 0 || minutes > 59);
        int seconds;
        do {
            seconds = enterIntNumber("Enter 0 <= seconds <= 59: ");
        } while (seconds < 0 || seconds > 59);
        System.out.println(hours + " hours " + minutes + " minutes " + seconds + " seconds");
        System.out.println("Dergees for 12 clock-face: " + getHoursArrowDegree(hours, minutes, seconds));
    }

    public static double getHoursArrowDegree(int hours, int minutes, int seconds) {
        return (hours >= 12 ? hours - 12 : hours) * 30 + minutes * 0.5 + seconds / 120;
    }
}