package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by anglomilian on 01/05/2017.
 */
public class TaskCh05N038 {

    static final int ITERATIONS = 2;

    public static void main(String[] args) {
        double[] movements = calculateHusbandMovements(ITERATIONS);
        System.out.printf("a) Distance from home: %3.3f %s %n", movements[1], "km");
        System.out.printf("b) Husband has walked: %3.3f %s", movements[0], "km");
    }

    public static double[] calculateHusbandMovements(int iterations) {
        double[] movements = new double[2];
        for (double i = 1; i <= iterations; i++) {
            movements[0] += 1 / i;
            if (i % 2 == 0) {
                movements[1] -= 1.0 / i;
            } else {
                movements[1] += 1.0 / i;
            }
        }
        return movements;
    }
}
