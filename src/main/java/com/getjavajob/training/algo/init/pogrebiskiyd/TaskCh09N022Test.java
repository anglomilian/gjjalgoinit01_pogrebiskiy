package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N022.getHalfWord;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N022Test {

    public static void main(String[] args) {
        testGetHalfWord();
    }

    private static void testGetHalfWord() {
        assertEquals("TaskCh09N022Test.testGetHalfWord", "nozd", getHalfWord("nozdormu"));
    }
}
