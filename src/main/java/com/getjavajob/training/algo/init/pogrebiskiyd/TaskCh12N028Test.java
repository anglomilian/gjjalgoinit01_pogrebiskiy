package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N028.arraySnake;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 3/8/2017.
 */

public class TaskCh12N028Test {

    public static void main(String[] args) {
        testArraySnake();
    }

    private static void testArraySnake() {
        int[][] testSnake = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("TaskCh12N028Test.testArraySnake", testSnake, arraySnake(5));
    }
}
