package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh04N115 {

    private static final int ANIMALS_CYCLE = 12; // rat cow tiger rabbit dragon snake horse sheep monkey cock dog pig
    private static final int COLORS_CYCLE = 10; // green red yellow white black

    public static void main(String[] args) {
        int year;
        do {
            year = enterIntNumber("Enter year >= 0: ");
        } while (year < 0);
        System.out.println(getChinaYear(year));
    }

    public static String getChinaYear(int year) {
        String result;
        int zeroPointColor = year - COLORS_CYCLE * (year / COLORS_CYCLE);
        switch (zeroPointColor) {
            case 0:
            case 1:
                result = "White ";
                break;
            case 2:
            case 3:
                result = "Black ";
                break;
            case 4:
            case 5:
                result = "Green ";
                break;
            case 6:
            case 7:
                result = "Red ";
                break;
            case 8:
            case 9:
                result = "Yellow ";
                break;
            default:
                result = "Wrong color number";
        }
        int zeroPointYear = year - ANIMALS_CYCLE * (year / ANIMALS_CYCLE);
        switch (zeroPointYear) {
            case 0:
                result += "Monkey";
                break;
            case 1:
                result += "Cock";
                break;
            case 2:
                result += "Dog";
                break;
            case 3:
                result += "Pig";
                break;
            case 4:
                result += "Rat";
                break;
            case 5:
                result += "Cow";
                break;
            case 6:
                result += "Tiger";
                break;
            case 7:
                result += "Rabbit";
                break;
            case 8:
                result += "Dragon";
                break;
            case 9:
                result += "Snake";
                break;
            case 10:
                result += "Horse";
                break;
            case 11:
                result += "Sheep";
                break;
            default:
                result = "Wrong animal number";
        }
        return result;
    }
}