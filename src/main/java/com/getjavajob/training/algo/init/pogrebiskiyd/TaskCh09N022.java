package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N022 {

    public static void main(String[] args) {
        String oneWord;
        do {
            oneWord = enterString("Enter one word with even length: ");
        } while (oneWord.contains(" ") || oneWord.length() % 2 != 0);
        System.out.println("1/2 of " + oneWord + " is: " + getHalfWord(oneWord));
    }

    public static String getHalfWord(String oneWord) {
        return oneWord.substring(0, oneWord.length() / 2);
    }
}