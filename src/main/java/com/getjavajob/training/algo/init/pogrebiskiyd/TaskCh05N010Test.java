package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh05N010.calculateCurrencyValues;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh05N010Test {

    public static void main(String[] args) {
        testCalculateCurrencyValues();
    }

    private static void testCalculateCurrencyValues() {
        double[] testValues = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200};
        assertEquals("TaskCh05N010Test.testCalculateCurrencyValues", testValues, calculateCurrencyValues(10));
    }
}
