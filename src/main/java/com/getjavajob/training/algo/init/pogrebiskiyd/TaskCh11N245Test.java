package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh11N245.negativeElementsFirst;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 3/4/2017.
 */

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testNegativeElementsFirst();
    }

    private static void testNegativeElementsFirst() {
        int[] testinput = {8, 54, 7, 15, -8, -5, 69, 78, 42, 765, 23, -9, 765, -4, -67, 58, 89, 456, 123, -87};
        int[] testExpected = {-8, -5, -9, -4, -67, -87, 123, 456, 89, 58, 765, 23, 765, 42, 78, 69, 15, 7, 54, 8};
        assertEquals("TaskCh11N245Test.testNegativeElementsFirst", testExpected, negativeElementsFirst(testinput));
    }
}