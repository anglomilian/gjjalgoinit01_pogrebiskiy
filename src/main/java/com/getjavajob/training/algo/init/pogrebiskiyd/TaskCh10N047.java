package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N047 {

    public static void main(String[] args) {
        int quantity;
        do {
            quantity = enterIntNumber("Enter quantity of the members: ");
        } while (quantity < 1);
        System.out.println(getKMemeberOfFibbonachiRow(quantity));
    }

    public static int getKMemeberOfFibbonachiRow(int quantity) {
        if (quantity == 1 || quantity == 2) {
            return 1;
        }
        return getKMemeberOfFibbonachiRow(quantity - 1) + getKMemeberOfFibbonachiRow(quantity - 2);
    }
}