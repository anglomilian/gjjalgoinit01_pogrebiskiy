package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

/**
 * Created by @author anglomilian on 2/20/2017.
 */
public class TaskCh02N031 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter an integer 100 <= number <= 999: ");
        } while (number < 100 || number > 999);
        int xNumber = findX(number);
        System.out.print("Number X is: " + xNumber);
    }

    public static int findX(int number) {
        int first = number / 100;
        int second = (number - first * 100) / 10;
        int third = (number - first * 100) % 10;
        return first * 100 + third * 10 + second;
    }
}