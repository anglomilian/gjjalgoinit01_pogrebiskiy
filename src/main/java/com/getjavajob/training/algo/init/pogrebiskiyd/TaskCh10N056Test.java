package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N056.isSimpleNumber;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N056Test {

    public static void main(String[] args) {
        testIsSimpleNumber();
    }

    private static void testIsSimpleNumber() {
        assertEquals("TaskCh10N056Test.testIsSimpleNumber", true, isSimpleNumber(997, 2));
        assertEquals("TaskCh10N056Test.testIsSimpleNumber", false, isSimpleNumber(555, 2));
    }
}
