package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Task 3.29 <br>
 * a) every number of X and Y is odd <br>
 * b) only one of the numbers X and Y is lower than 20 <br>
 * v) at least one of the numbers X and Y is equal 0 <br>
 * g) every number of X, Y, Z is negative <br>
 * d) only one of the numbers X, Y and Z is multiple 5 <br>
 * e) at least one of the numbers X, Y, Z is greater than 100 <br>
 *
 * @author Anglomilian
 * @version 3.0
 */


public class TaskCh03N029 {

    public static void main(String[] args) {
        System.out.println("Expression A: " + checkAExpression(23, 11));
        System.out.println("Expression B: " + checkBExpression(23, 11));
        System.out.println("Expression V: " + checkVExpression(23, 0));
        System.out.println("Expression G: " + checkGExpression(-23, -11, -45));
        System.out.println("Expression D: " + checkDExpression(24, 21, 25));
        System.out.println("Expression E: " + checkEExpression(25, 110, 78));
    }

    public static boolean checkAExpression(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    public static boolean checkBExpression(int x, int y) {
        return x < 20 ^ y < 20;
    }

    public static boolean checkVExpression(int x, int y) {
        return x == 0 || y == 0;
    }

    public static boolean checkGExpression(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    public static boolean checkDExpression(int x, int y, int z) {
        //x % 5 == 0 ^ y % 5 == 0 ^ z % 5 == 0 ^ (x % 5 == 0 && y % 5 == 0 && z % 5 == 0)
        boolean xDivFive = x % 5 == 0; //first comparsion
        boolean yDivFive = y % 5 == 0; //second
        boolean zDivFive = z % 5 == 0; //third
        boolean xyzDivFive = xDivFive && yDivFive && zDivFive;
        return xDivFive ^ yDivFive ^ zDivFive ^ xyzDivFive;
    }

    public static boolean checkEExpression(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}