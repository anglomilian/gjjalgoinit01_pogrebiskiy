package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printOneDimArray;

public class TaskCh10N053 {

    public static void main(String[] args) {
        int[] forReverse = {3545, 45, 5345, 768, 545, 5454, 5472, 6577, 5455, 3248, 4547, 1534, 5461, 1356};
        printOneDimArray("Array before reverse:", forReverse);
        int[] reversed = reverseArray(forReverse.clone());
        printOneDimArray("Array after reverse:", reversed);
    }

    public static int[] reverseArray(int[] forReverse) {
        reverseArray(forReverse, forReverse.length - 1);
        return forReverse;
    }

    private static void reverseArray(int[] forReverse, int index) {
        if (index > forReverse.length / 2) {
            int temp = forReverse[forReverse.length - index - 1];
            forReverse[forReverse.length - index - 1] = forReverse[index];
            forReverse[index] = temp;
            reverseArray(forReverse, index - 1);
        }
    }
}