package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.*;

public class TaskCh12N234 {
    private static final int SIZE_N = 4;
    private static final int SIZE_M = 5;

    public static void main(String[] args) {
        int rowNumber;
        do {
            rowNumber = enterIntNumber("Enter number of the 0<= row <= " + (SIZE_M - 1) + " to delete: ");
        } while (rowNumber < 0 || rowNumber > SIZE_M - 1);
        int[][] raid = fillTwoDimArrayRandom(SIZE_N, SIZE_M, 50);
        printTwoDimArray("Array before deleting row:", raid);
        int[][] raidDel = delArRow(raid.clone(), rowNumber);
        printTwoDimArray("Array after deleting row:", raidDel);

        int stringNumber;
        do {
            stringNumber = enterIntNumber("Enter number of the 0 <= string <= " + (SIZE_N - 1) + " to delete: ");
        } while (stringNumber < 0 || stringNumber > SIZE_N - 1);
        raid = fillTwoDimArrayRandom(SIZE_N, SIZE_M, 50);
        printTwoDimArray("Array before deleting string:", raid);
        raidDel = delArString(raid.clone(), stringNumber);
        printTwoDimArray("Array after deleting string:", raidDel);
    }

    public static int[][] delArRow(int[][] a, int rowNumber) {
        for (int i = 0; i < a.length; i++) {
            a[i][rowNumber] = 0;
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = rowNumber; j < a[0].length - 1; j++) {
                int t = a[i][j + 1];
                a[i][j + 1] = a[i][j];
                a[i][j] = t;
            }
        }
        return a;
    }

    public static int[][] delArString(int[][] a, int stringNumber) {
        for (int j = 0; j < a[0].length; j++) {
            a[stringNumber][j] = 0;
        }
        for (int i = stringNumber; i < a.length - 1; i++) {
            for (int j = 0; j < a[0].length; j++) {
                int t = a[i + 1][j];
                a[i + 1][j] = a[i][j];
                a[i][j] = t;
            }
        }
        return a;
    }
}