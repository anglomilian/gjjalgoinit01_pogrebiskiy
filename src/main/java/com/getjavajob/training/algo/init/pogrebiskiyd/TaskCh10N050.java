package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N050 {

    public static void main(String[] args) {
        // wikipedia definition
        // n + 1                     m = 0
        // a(m - 1, 1)               m > 0, n = 0
        // a((m - 1), a(m, n - 1))   m > 0, n > 0
        int m = enterIntNumber("Enter m >= 0: ");
        int n = enterIntNumber("Enter n >= 0: ");
        System.out.println(akerman(m, n));
    }

    public static int akerman(int m, int n) {
        if (m != 0) {
            if (n == 0 && m > 0) {
                return akerman(m - 1, 1);
            } else {
                return akerman(m - 1, akerman(m, n - 1));
            }
        } else {
            return n + 1;
        }
    }
}
