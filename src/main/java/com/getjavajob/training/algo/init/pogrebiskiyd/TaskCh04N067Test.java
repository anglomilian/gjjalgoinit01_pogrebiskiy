package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N067.checkDayType;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh04N067Test {

    public static void main(String[] args) {
        testCheckDayType();
    }

    private static void testCheckDayType() {
        assertEquals("TaskCh04N067Test.testCheckDayType", "workday", checkDayType(5));
        assertEquals("TaskCh04N067Test.testCheckDayType", "weekend", checkDayType(7));
    }
}
