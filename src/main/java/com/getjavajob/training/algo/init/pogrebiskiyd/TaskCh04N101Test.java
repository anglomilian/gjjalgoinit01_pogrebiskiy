package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N101.checkSeason;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh04N101Test {
    public static void main(String[] args) {
        testCheckSeason();
    }

    private static void testCheckSeason() {
        assertEquals("TaskCh04N101Test.testCheckDayType", "winter", checkSeason(1));
        assertEquals("TaskCh04N101Test.testCheckDayType", "winter", checkSeason(2));
        assertEquals("TaskCh04N101Test.testCheckDayType", "spring", checkSeason(3));
        assertEquals("TaskCh04N101Test.testCheckDayType", "spring", checkSeason(4));
        assertEquals("TaskCh04N101Test.testCheckDayType", "spring", checkSeason(5));
        assertEquals("TaskCh04N101Test.testCheckDayType", "summer", checkSeason(6));
        assertEquals("TaskCh04N101Test.testCheckDayType", "summer", checkSeason(7));
        assertEquals("TaskCh04N101Test.testCheckDayType", "summer", checkSeason(8));
        assertEquals("TaskCh04N101Test.testCheckDayType", "autumn", checkSeason(9));
        assertEquals("TaskCh04N101Test.testCheckDayType", "autumn", checkSeason(10));
        assertEquals("TaskCh04N101Test.testCheckDayType", "autumn", checkSeason(11));
        assertEquals("TaskCh04N101Test.testCheckDayType", "winter", checkSeason(12));
    }
}
