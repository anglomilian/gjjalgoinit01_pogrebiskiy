package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N166 {

    public static void main(String[] args) {
        String inputLine;
        do {
            inputLine = enterString("Enter several words: ");
        } while (!inputLine.contains(" ") || inputLine.contains("-"));
        System.out.print(swapFirstLastWords(inputLine));
    }

    public static String swapFirstLastWords(String inputLine) {
        String[] words = inputLine.split(" ");
        if (words.length >= 10) {
            return "Expression length should be not more than 10 words.";
        }
        StringBuilder concat = new StringBuilder();
        concat.append(words[words.length - 1]).append(" ");
        for (int i = 1; i < words.length - 1; i++) {
            concat.append(words[i]).append(" ");
        }
        concat.append(words[0]);
        return concat.toString();
    }
}
