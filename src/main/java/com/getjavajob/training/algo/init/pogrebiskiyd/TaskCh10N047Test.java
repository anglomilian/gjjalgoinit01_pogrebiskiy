package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N047.getKMemeberOfFibbonachiRow;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N047Test {

    public static void main(String[] args) {
        testGetKMemeberOfFibbonachiRow();
    }

    private static void testGetKMemeberOfFibbonachiRow() {
        assertEquals("TaskCh10N047Test.testGetKMemeberOfFibbonachiRow", 55, getKMemeberOfFibbonachiRow(10));
        assertEquals("TaskCh10N047Test.testGetKMemeberOfFibbonachiRow", 832040, getKMemeberOfFibbonachiRow(30));
    }
}
