package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh02N013 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter an integer 100 < number < 200: ");
        } while (number < 101 || number > 199);

        int bcwrdNumber = getBackwardNumber(number);
        System.out.println("Backward number is: " + bcwrdNumber);
    }

    public static int getBackwardNumber(int number) {
        return number % 10 * 100 + (number - 100) / 10 * 10 + 1;
    }
}
