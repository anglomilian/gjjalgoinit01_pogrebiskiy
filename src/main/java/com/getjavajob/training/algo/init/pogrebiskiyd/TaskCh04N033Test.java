package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N033.isEven;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N033.isOdd;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsOdd();
        testIsEven();
    }

    private static void testIsOdd() {
        assertEquals("TaskCh04N033Test.testIsOdd", false, isOdd(22));
        assertEquals("TaskCh04N033Test.testIsOdd", true, isOdd(27));
        System.out.println();
    }

    private static void testIsEven() {
        assertEquals("TaskCh04N033Test.testIsEven", false, isEven(27));
        assertEquals("TaskCh04N033Test.testIsEven", true, isEven(22));
    }
}