package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh05N038.calculateHusbandMovements;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by anglomilian on 01/05/2017.
 */
public class TaskCh05N038Test {

    public static void main(String[] args) {
        testCalculateHusbandMovements();
    }

    public static void testCalculateHusbandMovements() {
        double[] expectedMovements = {5.187377517639621, 0.688172179310195};
        assertEquals("TaskCh05N058Test.testCalculateHusbandMovements", expectedMovements, calculateHusbandMovements(100));
    }
}
