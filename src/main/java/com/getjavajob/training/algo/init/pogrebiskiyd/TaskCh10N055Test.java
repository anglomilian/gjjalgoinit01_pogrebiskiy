package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N055.nBaseConverter;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N055Test {

    public static void main(String[] args) {
        testNBaseConverter();
    }

    private static void testNBaseConverter() {
        assertEquals("TaskCh10N055Test.testNBaseConverter", "74C05", nBaseConverter(478213, 16));
        assertEquals("TaskCh10N055Test.testNBaseConverter", "1646005", nBaseConverter(478213, 8));
        assertEquals("TaskCh10N055Test.testNBaseConverter", "1110100110000000101", nBaseConverter(478213, 2));
    }
}