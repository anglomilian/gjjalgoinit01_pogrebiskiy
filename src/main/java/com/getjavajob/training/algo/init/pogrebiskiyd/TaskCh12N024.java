package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printTwoDimArray;

public class TaskCh12N024 {

    public static void main(String[] args) {
        int arrSize = 6;
        int[][] output = sumConers(arrSize);
        printTwoDimArray("A", output);
        output = nDiagonal(arrSize);
        printTwoDimArray("B", output);
    }

    public static int[][] sumConers(int arrSize) {
        int[][] output = new int[arrSize][arrSize];
        for (int i = 0; i < output.length; i++) {
            for (int j = 0; j < output.length; j++) {
                if (i == 0 || j == 0) {
                    output[i][j] = 1;
                } else {
                    output[i][j] = output[i - 1][j] + output[i][j - 1];
                }
            }
        }
        return output;
    }

    public static int[][] nDiagonal(int arrSize) {
        int[][] output = new int[arrSize][arrSize];
        for (int i = 0; i < output.length; i++) {
            for (int j = 0; j < output.length; j++) {
                output[i][j] = ((i + j) % arrSize) + 1;
            }
        }
        return output;
    }
}