package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N017.isFirstLastLettersEqual;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N017Test {
    public static void main(String[] args) {
        testSwapFirstLast();
    }

    private static void testSwapFirstLast() {
        assertEquals("TaskCh09N017Test.testSwapFirstLast", true, isFirstLastLettersEqual("alextraza"));
        assertEquals("TaskCh09N017Test.testSwapFirstLast", false, isFirstLastLettersEqual("ysera"));
    }
}
