package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by @author anglomilian on 3/14/2017.
 */

import java.util.ArrayList;
import java.util.List;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.Database.getEmployeesWorkedForYears;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.Database.searchBySubstring;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

class TaskCh13N012Test {

    public static void main(String[] args) {
        testSearchBySubstring();
        testGetEmployeesWhoContainsString();
    }

    private static void testSearchBySubstring() {
        Database.fillDatabase();
        List<Employee> expectedEmployees = new ArrayList<>();
        expectedEmployees.add(new Employee("Andranik", "Karenovich", "Hadjabekyan", "Frolova,45-8", 3, 2015));
        expectedEmployees.add(new Employee("Dmitriy", "Fedorovich", "Karamazov", "Voskhodnaya,56-67", 4, 2016));
        expectedEmployees.add(new Employee("Ivan", "Fedorovich", "Karamazov", "Neglinnaya,34-90", 2, 2014));
        expectedEmployees.add(new Employee("Alexey", "Fedorovich", "Karamazov", "Prazhskaya,45-71", 10, 2015));
        assertEquals("TaskCh13N012Test.testSearchBySubstring", expectedEmployees, searchBySubstring("kar"));
    }

    private static void testGetEmployeesWhoContainsString() {
        List<Employee> expectedEmployees = new ArrayList<>();
        expectedEmployees.add(new Employee("Illidan", "Storm", "Rage", "Black Temple", 10, 1999));
        expectedEmployees.add(new Employee("Tyrande", "Wisper", "Wind", "Darnassus", 5, 2011));
        assertEquals("TaskCh13N012Test.testGetEmployeesWhoContainsString", expectedEmployees, getEmployeesWorkedForYears(5, 3, 2017));
    }
}