package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N053.reverseArray;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 29/03/17.
 */

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testReverseArray();
    }

    private static void testReverseArray() {
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] actual = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh10N053Test.testReverseArray", actual, reverseArray(expected));
    }
}
