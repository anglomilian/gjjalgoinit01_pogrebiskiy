package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 27/02/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh03N029.*;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh03N029Test {

    public static void main(String[] args) {
        testCheckAExpression();
        testCheckBExpression();
        testCheckVExpression();
        testCheckGExpression();
        testCheckDExpression();
        testCheckEExpression();
    }

    private static void testCheckAExpression() {
        assertEquals("TaskCh03N029Test.testCheckAExpression", false, checkAExpression(21, 10));
        assertEquals("TaskCh03N029Test.testCheckAExpression", false, checkAExpression(40, 83));
        assertEquals("TaskCh03N029Test.testCheckAExpression", false, checkAExpression(40, 80));
        assertEquals("TaskCh03N029Test.testCheckAExpression", true, checkAExpression(47, 33));
        System.out.println();
    }

    private static void testCheckBExpression() {
        assertEquals("TaskCh03N029Test.testCheckBExpression", false, checkBExpression(21, 50));
        assertEquals("TaskCh03N029Test.testCheckBExpression", false, checkBExpression(10, 13));
        assertEquals("TaskCh03N029Test.testCheckBExpression", true, checkBExpression(17, 33));
        assertEquals("TaskCh03N029Test.testCheckBExpression", true, checkBExpression(77, 13));
        System.out.println();
    }

    private static void testCheckVExpression() {
        assertEquals("TaskCh03N029Test.testCheckVExpression", false, checkVExpression(21, 50));
        assertEquals("TaskCh03N029Test.testCheckVExpression", true, checkVExpression(0, 13));
        assertEquals("TaskCh03N029Test.testCheckVExpression", true, checkVExpression(17, 0));
        assertEquals("TaskCh03N029Test.testCheckVExpression", true, checkVExpression(0, 0));
        System.out.println();
    }

    private static void testCheckGExpression() {
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(21, 50, 33));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(22, 13, -17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(22, -13, 17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(-22, 13, 17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(22, -13, -17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(-22, 13, -17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", false, checkGExpression(-22, -13, 17));
        assertEquals("TaskCh03N029Test.testCheckGExpression", true, checkGExpression(-22, -13, -17));
        System.out.println();
    }

    private static void testCheckDExpression() {
        assertEquals("TaskCh03N029Test.testCheckDExpression", false, checkDExpression(21, 17, 33));
        assertEquals("TaskCh03N029Test.testCheckDExpression", false, checkDExpression(55, 25, 30));
        assertEquals("TaskCh03N029Test.testCheckDExpression", false, checkDExpression(55, 25, 17));
        assertEquals("TaskCh03N029Test.testCheckDExpression", false, checkDExpression(22, 25, 30));
        assertEquals("TaskCh03N029Test.testCheckDExpression", false, checkDExpression(55, 13, 30));
        assertEquals("TaskCh03N029Test.testCheckDExpression", true, checkDExpression(55, 13, 17));
        assertEquals("TaskCh03N029Test.testCheckDExpression", true, checkDExpression(22, 25, 17));
        assertEquals("TaskCh03N029Test.testCheckDExpression", true, checkDExpression(22, 13, 30));
        System.out.println();
    }

    private static void testCheckEExpression() {
        assertEquals("TaskCh03N029Test.testCheckEExpression", false, checkEExpression(21, 17, 33));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(550, 25, 30));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(55, 250, 17));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(22, 25, 300));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(550, 130, 30));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(55, 130, 170));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(220, 25, 170));
        assertEquals("TaskCh03N029Test.testCheckEExpression", true, checkEExpression(220, 130, 300));
        System.out.println();
    }
}