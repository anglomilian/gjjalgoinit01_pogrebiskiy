package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N107 {

    public static void main(String[] args) {
        String oneWord;
        do {
            oneWord = enterString("Enter one word: ");//"onixia";
        } while (oneWord.contains(" "));
        System.out.println(swapFirstALastO(oneWord));
    }

    public static String swapFirstALastO(String oneWord) {
        int firstA = oneWord.indexOf('a');
        int lastO = oneWord.lastIndexOf('o');
        if (firstA >= 0 && lastO >= 0) {
            StringBuilder sb = new StringBuilder(oneWord);
            sb.setCharAt(firstA, 'o');
            sb.setCharAt(lastO, 'a');
            return sb.toString();
        } else {
            return "Letters cannot be swapped";
        }
    }
}