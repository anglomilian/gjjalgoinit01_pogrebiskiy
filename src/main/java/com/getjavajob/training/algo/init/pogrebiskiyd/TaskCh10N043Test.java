package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N043.numberOfDigits;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N043.sumOfDigits;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh10N043Test {

    public static void main(String[] args) {
        testSumOfDigits();
        testNumberOfDigits();
    }

    private static void testSumOfDigits() {
        assertEquals("TaskCh10N043Test.testSumOfDigits", 44, sumOfDigits(54789146));
        assertEquals("TaskCh10N043Test.testSumOfDigits", 1, sumOfDigits(100000));
        System.out.println();
    }

    private static void testNumberOfDigits() {
        assertEquals("TaskCh10N043Test.testNumberOfDigits", 6, numberOfDigits(100000));
    }
}
