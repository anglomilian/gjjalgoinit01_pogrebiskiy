package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N052 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter number > 1: ");
        } while (number < 1);
        System.out.println(reverseNumber(number));
    }

    public static long reverseNumber(int number) {
        return reverseNumber(number, 0);
    }

    private static long reverseNumber(int number, int m) {
        return number == 0 ? m : reverseNumber(number / 10, m * 10 + number % 10);
    }
}