package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N044 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter number > 1: ");
        } while (number < 1);
        System.out.println(number + " digital root is: " + getNumberDigitalRoot(number));
    }

    public static int getNumberDigitalRoot(int number) {
        return number < 10 ? number : getNumberDigitalRoot(sumOfDigits(number));
    }

    public static int sumOfDigits(int number) {
        return number < 10 ? number : number % 10 + sumOfDigits(number / 10);
    }
}
