package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N044.getNumberDigitalRoot;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N044.sumOfDigits;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N044Test {

    public static void main(String[] args) {
        testGetNumberDigitalRoot();
        testSumOfDigits();
    }

    private static void testGetNumberDigitalRoot() {
        assertEquals("TaskCh10N044Test.testGetNumberDigitalRoot", 8, getNumberDigitalRoot(54789146));
        assertEquals("TaskCh10N044Test.testGetNumberDigitalRoot", 3, getNumberDigitalRoot(3432423));
        System.out.println();
    }

    private static void testSumOfDigits() {
        assertEquals("TaskCh10N044Test.testSumOfDigits", 44, sumOfDigits(54789146));
        assertEquals("TaskCh10N044Test.testSumOfDigits", 45, sumOfDigits(99999));
    }
}
