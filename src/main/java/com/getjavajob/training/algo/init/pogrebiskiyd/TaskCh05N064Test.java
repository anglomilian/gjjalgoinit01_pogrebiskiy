package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 27/02/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh05N064.calculateDensity;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh05N064Test {

    public static void main(String[] args) {
        testCalculateDensity();
    }

    private static void testCalculateDensity() {
        int[][] districts = {{120, 20}, {200, 40}, {800, 110}, {750, 30}, {900, 20}, {400, 50},
                {500, 80}, {400, 30}, {300, 100}, {1000, 70}, {600, 60}, {550, 30}};
        assertEquals("TaskCh05N064Test.testCalculateDensity", 10.324193548387097, calculateDensity(districts));
    }
}
