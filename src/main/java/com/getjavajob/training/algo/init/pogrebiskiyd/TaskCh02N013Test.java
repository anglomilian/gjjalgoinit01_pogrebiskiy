package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh02N013.getBackwardNumber;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 2/23/2017.
 */
// get backward number for 100<n<200. Example: for n = 153 backward number is 351
public class TaskCh02N013Test {
    public static void main(String[] args) {
        testGetBackwardNumber();
    }

    private static void testGetBackwardNumber() {
        assertEquals("TaskCh02N013Test.testGetBackwardNumber", 351, getBackwardNumber(153));
        assertEquals("TaskCh02N013Test.testGetBackwardNumber", 111, getBackwardNumber(111));
    }
}
