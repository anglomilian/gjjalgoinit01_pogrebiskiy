package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh02N031.findX;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 2/23/2017.
 */
// find x for number 100 <= n <= 999. swap second and last digits of number n to find x.
// Example: for n=853 x is 835
public class TaskCh02N031Test {
    public static void main(String[] args) {
        testFindX();
    }

    private static void testFindX() {
        assertEquals("TaskCh02N031Test.testFindX", 835, findX(853));
        assertEquals("TaskCh02N031Test.testFindX", 110, findX(101));
    }
}