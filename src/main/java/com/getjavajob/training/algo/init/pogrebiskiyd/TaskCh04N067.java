package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh04N067 {

    public static void main(String[] args) {
        // 1 <= dayNumber <= 365; 1 of January is Monday. Is dayNumber a weekend day or a work day?
        int dayNumber;
        do {
            dayNumber = enterIntNumber("Enter 1 <= day <= 365: ");
        } while (dayNumber < 1 || dayNumber > 365);
        System.out.println(checkDayType(dayNumber));
    }

    public static String checkDayType(int dayNumber) {
        int r = dayNumber % 7;
        return r > 0 && r < 6 ? "workday" : "weekend";
    }
}