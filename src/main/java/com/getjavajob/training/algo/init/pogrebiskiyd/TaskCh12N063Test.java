package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N063.getMiddle;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 3/8/2017.
 */

public class TaskCh12N063Test {

    public static void main(String[] args) {
        testGetMiddle();
    }

    private static void testGetMiddle() {
        int[][] testParallels = new int[][]{
                {27, 25, 29, 28},
                {34, 29, 34, 30},
                {28, 26, 34, 30},
                {32, 27, 27, 30},
                {25, 32, 27, 25},
                {28, 28, 26, 25},
                {29, 25, 28, 30},
                {26, 26, 25, 34},
                {28, 29, 29, 30},
                {27, 25, 25, 33},
                {31, 33, 29, 26},
        };
        int[] testMiddle = new int[]{
                27, 31, 29, 29, 27, 26, 28, 27, 29, 27, 29
        };
        assertEquals("TaskCh12N063Test.testGetMiddle", testMiddle, getMiddle(testParallels));
    }
}
