package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.*;

public class TaskCh12N063 {

    private static final int CLASSES_NUMBER = 11;
    private static final int CLASSES_IN_PARALLEL = 4;

    public static void main(String[] args) {
        int[][] parallels = fillTwoDimArrayRandom(CLASSES_NUMBER, CLASSES_IN_PARALLEL, 10);
        printTwoDimArray("Parallels:", parallels);
        int[] middleNumber = getMiddle(parallels);
        printOneDimArray("Middle numbers of pupils in an each parallel: ", middleNumber);
    }

    public static int[] getMiddle(int[][] parallels) {
        int[] middleNumber = new int[CLASSES_NUMBER];
        int summ = 0;
        for (int i = 0; i < parallels.length; i++) {
            for (int j = 0; j < parallels[0].length; j++) {
                summ += parallels[i][j];
            }
            middleNumber[i] = summ / CLASSES_IN_PARALLEL;
            summ = 0;
        }
        return middleNumber;
    }
}