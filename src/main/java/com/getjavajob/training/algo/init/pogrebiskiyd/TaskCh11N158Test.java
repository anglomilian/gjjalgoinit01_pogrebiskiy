package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 03/03/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh11N158.killTwinsElements;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh11N158Test {

    public static void main(String[] args) {
        testKillTwinsElements();
    }

    private static void testKillTwinsElements() {
        int[] testInput = {25, 45, 76, 98, 31, 25, 25, 34, 41, 56, 92, 34, 87, 33, 25, 78, 76, 89, 87, 78, 90};
        int[] testOutput = {25, 45, 76, 98, 31, 34, 41, 56, 92, 87, 33, 78, 89, 90, 0, 0, 0, 0, 0, 0, 0};

        assertEquals("TaskCh11N158Test.testKillTwinsElements", testOutput, killTwinsElements(testInput));
    }
}
