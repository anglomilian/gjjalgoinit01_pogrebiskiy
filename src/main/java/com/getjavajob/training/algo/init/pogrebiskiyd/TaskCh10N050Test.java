package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

/**
 * Created by denis on 01/03/17.
 */

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N050.akerman;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

public class TaskCh10N050Test {

    public static void main(String[] args) {
        testAkerman();
    }

    private static void testAkerman() {
        assertEquals("TaskCh10N050Test.testAkerman", 1, akerman(0, 0));
        assertEquals("TaskCh10N050Test.testAkerman", 3, akerman(2, 0));
        assertEquals("TaskCh10N050Test.testAkerman", 253, akerman(3, 5));
    }
}
