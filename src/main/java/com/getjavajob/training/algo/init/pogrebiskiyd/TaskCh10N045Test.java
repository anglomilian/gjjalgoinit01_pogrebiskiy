package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N045.nMemberOfArithmeticProgression;
import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N045.sumNNumbersOfArithmeticProgression;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 01/03/17.
 */

public class TaskCh10N045Test {

    public static void main(String[] args) {
        testNMemberOfArithmeticProgression();
        testSumNNumbersOfArithmeticProgression();
    }

    private static void testNMemberOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testNMemberOfArithmeticProgression", 10, nMemberOfArithmeticProgression(1, 1, 10));
        assertEquals("TaskCh10N045Test.testNMemberOfArithmeticProgression", 95, nMemberOfArithmeticProgression(5, 10, 10));
        System.out.println();
    }

    private static void testSumNNumbersOfArithmeticProgression() {
        assertEquals("TaskCh10N045Test.testSumNNumbersOfArithmeticProgression", 55, sumNNumbersOfArithmeticProgression(1, 1, 10));
        assertEquals("TaskCh10N045Test.testSumNNumbersOfArithmeticProgression", 500, sumNNumbersOfArithmeticProgression(5, 10, 10));
    }
}
