package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N017 {

    public static void main(String[] args) {
        String oneWord;
        do {
            oneWord = enterString("Enter one word: ");
        } while (oneWord.contains(" "));
        System.out.println(oneWord + " " + isFirstLastLettersEqual(oneWord));
    }

    public static boolean isFirstLastLettersEqual(String s) {
        return s.charAt(0) == s.charAt(s.length() - 1);
    }
}
