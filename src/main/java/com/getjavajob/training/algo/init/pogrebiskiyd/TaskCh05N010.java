package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterDoubleNumber;

public class TaskCh05N010 {
    public static void main(String[] args) {
        double currency;
        do {
            currency = enterDoubleNumber("Enter currency value: ");
        } while (currency < 0);
        printCurrencyTable(calculateCurrencyValues(currency));
    }

    public static double[] calculateCurrencyValues(double currency) {
        double[] currencyValues = new double[20];
        for (int i = 0; i < currencyValues.length; i++) {
            currencyValues[i] = (i + 1) * currency;
        }
        return currencyValues;
    }

    private static void printCurrencyTable(double[] currencyValues) {
        int i;
        for (i = 1; i <= currencyValues.length; i++) {
            System.out.printf("%8d ", i);
        }
        System.out.println();
        for (i = 0; i < currencyValues.length; i++) {
            System.out.printf("%8.2f ", currencyValues[i]);
        }
    }
}