package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N055 {

    private static final String NSYSTEMABC = "0123456789ABCDEF";

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter number >= 0: ");
        } while (number < 0);
        int nSysBase;
        do {
            nSysBase = enterIntNumber("Enter numeral system base 2 <= n <= 16: ");
        } while (nSysBase < 2 || nSysBase > 16);
        System.out.println("Your number : " + nBaseConverter(number, nSysBase));
    }

    public static String nBaseConverter(int number, int nSysBase) {
        return number == 0 ? "" : nBaseConverter(number / nSysBase, nSysBase) + NSYSTEMABC.charAt(number % nSysBase);
    }
}