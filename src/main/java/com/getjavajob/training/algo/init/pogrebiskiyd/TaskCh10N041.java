package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N041 {

    public static void main(String[] args) {
        int n;
        do {
            n = enterIntNumber("Enter n >= 1: ");
        } while (n < 1);
        System.out.println(n + " factorial: " + nFactorial(n));
    }

    public static int nFactorial(int n) {
        return n == 1 ? 1 : n * nFactorial(n - 1);
    }
}
