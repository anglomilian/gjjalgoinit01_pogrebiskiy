package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh02N043 {

    public static int checkDividingAb(int a, int b) {
        return a % b * (b % a) + 1;
    }

    public static void main(String[] args) {
        int a;
        do {
            a = enterIntNumber("Enter number a != 0: ");
        } while (a == 0);
        int b;
        do {
            b = enterIntNumber("Enter number b != 0: ");
        } while (b == 0);
        System.out.println(checkDividingAb(a, b));
    }
}
