package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N185 {

    public static void main(String[] args) {
        String expression = enterString("Enter expression: ");//"a+b*2*((f/t)*(2+b*a)-pi*(4+a*b)))";
        int[] parantesisCounter = checkParentheses(expression.trim());
        System.out.println(printAnswerA(parantesisCounter));
        System.out.println(printAnswerB(parantesisCounter));
    }

    public static int[] checkParentheses(String expression) {
        int[] parantesisCounter = new int[3];
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == ')') {
                if (parantesisCounter[1] == 0) {
                    parantesisCounter[2] = i + 1;
                }
                parantesisCounter[1]++;
            } else if (expression.charAt(i) == '(') {
                parantesisCounter[0]++;
            }
        }
        return parantesisCounter;
    }

    public static String printAnswerA(int[] parantesisCounter) {
        return parantesisCounter[0] == parantesisCounter[1] ? "yes" : "no";
    }

    public static String printAnswerB(int[] parantesisCounter) {
        if (parantesisCounter[1] > parantesisCounter[0]) {
            return "Here is excess right (close) parenthesis in your expression, pos:" + parantesisCounter[2];
        } else if (parantesisCounter[1] < parantesisCounter[0]) {
            int leftMinusRight = parantesisCounter[0] - parantesisCounter[1];
            return "Here is excess left (open) parenthesis in your expression, total:" + leftMinusRight;
        } else {
            return "Correct expression";
        }
    }
}
