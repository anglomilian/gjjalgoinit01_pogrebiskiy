package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static java.lang.Math.*;

/**
 * Created by @author anglomilian on 2/19/2017.
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        double x = 1.0;
        double a = 2.0;
        double b = 3.0;
        double c = 4.0;
        double o = sqrt(1 - pow(sin(x), 2));
        double p = 1 / sqrt(a * pow(x, 2) + b * x + c);
        double r = (sqrt(x + 1) + sqrt(x - 1)) / 2 * sqrt(x);
        double s = abs(x) + abs(x + 1);
    }
}
