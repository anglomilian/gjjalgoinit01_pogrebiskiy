package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh06N087Test {

    public static void main(String[] args) {
        testgameScore();
        testgameResult();
    }

    private static void testgameScore() {
        Game bigGame = new Game();
        bigGame.firstTeam.setName("Los Angeles Lakers");
        bigGame.secondTeam.setName("Chikago Bulls");
        bigGame.setFirstTeamScore(68);
        bigGame.setSecondTeamScore(97);
        assertEquals("TaskCh06N087Test.testGameScore", "Los Angeles Lakers score is: 68. Chikago Bulls score is: 97.",
                bigGame.score());
        System.out.println();
    }

    private static void testgameResult() {
        Game bigGame = new Game();
        bigGame.firstTeam.setName("Los Angeles Lakers");
        bigGame.secondTeam.setName("Chikago Bulls");
        bigGame.setFirstTeamScore(97);
        bigGame.setSecondTeamScore(97);
        assertEquals("TaskCh06N087Test.testgameResult", "Draw. Los Angeles Lakers score is: 97. Chikago Bulls score is: 97",
                bigGame.result());
        bigGame.setFirstTeamScore(103);
        bigGame.setSecondTeamScore(97);
        assertEquals("TaskCh06N087Test.testgameResult", "Los Angeles Lakers is a WINNER with score: 103. Chikago Bulls score is: 97",
                bigGame.result());
        bigGame.setFirstTeamScore(97);
        bigGame.setSecondTeamScore(68);
        assertEquals("TaskCh06N087Test.testgameResult", "Chikago Bulls is a WINNER with score: 97. Los Angeles Lakers score is: 68",
                bigGame.result());
    }
}