package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N107.swapFirstALastO;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapFirstOLastA();
    }

    private static void testSwapFirstOLastA() {
        assertEquals("TaskCh09N107Test.testSwapFirstALastO", "anixio", swapFirstALastO("onixia"));
        assertEquals("TaskCh09N107Test.testSwapFirstALastO", "Morozavo", swapFirstALastO("Morozova"));
        assertEquals("TaskCh09N107Test.testSwapFirstALastO", "Letters cannot be swapped", swapFirstALastO("fdsfdsfdfds"));
    }
}
