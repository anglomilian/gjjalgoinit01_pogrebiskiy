package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterDoubleNumber;

public class TaskCh04N036 {

    public static void main(String[] args) {
        double trafficTime;
        do {
            trafficTime = enterDoubleNumber("Enter time in minutes <= 60: ");
        } while (trafficTime < 0 || trafficTime > 60);
        System.out.println(getTrafficLightColor(trafficTime));

    }

    public static String getTrafficLightColor(double time) {
        return time % 5 >= 3 ? "red" : "green";
    }
}
