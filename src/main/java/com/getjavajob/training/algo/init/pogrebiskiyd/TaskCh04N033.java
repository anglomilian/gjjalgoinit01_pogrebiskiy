package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh04N033 {

    public static void main(String[] args) {
        int number;
        do {
            number = enterIntNumber("Enter an integer number > 0: ");
        } while (number <= 0);

        System.out.println(isOdd(number));
        System.out.println(isEven(number));
    }

    public static boolean isOdd(int number) {
        return number % 2 != 0;
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

}
