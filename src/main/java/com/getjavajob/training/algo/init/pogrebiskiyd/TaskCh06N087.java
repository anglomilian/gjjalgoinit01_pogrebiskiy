package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;
import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh06N087 {

    public static void main(String[] args) {
        Game bigGame = new Game();
        bigGame.play();
    }
}

class Game {

    public Team firstTeam = new Team();
    public Team secondTeam = new Team();
    private int firstTeamScore;
    private int secondTeamScore;

    public void setFirstTeamScore(int firstTeamScore) {
        this.firstTeamScore = firstTeamScore;
    }

    public void setSecondTeamScore(int secondTeamScore) {
        this.secondTeamScore = secondTeamScore;
    }

    void play() {
        firstTeam.setName(enterString("Enter team #1: "));
        secondTeam.setName(enterString("Enter team #2: "));
        int teamNumberToScore;
        do {
            do {
                teamNumberToScore = enterIntNumber("Enter team to score ( 1 or 2 or 0 to finish game): ");
            } while (teamNumberToScore < 0 || teamNumberToScore > 2);
            if (teamNumberToScore == 0) {
                System.out.println(result());
            } else {
                int teamPoints;
                do {
                    teamPoints = enterIntNumber("Enter score ( 1 or 2 or 3): ");
                } while (teamPoints < 1 || teamPoints > 3);
                if (teamNumberToScore == 1) {
                    firstTeamScore += teamPoints;
                } else {
                    secondTeamScore += teamPoints;
                }
                System.out.println(score());
            }
        } while (teamNumberToScore != 0);
    }

    String score() {
        return firstTeam.getName() + " score is: " + firstTeamScore + ". " + secondTeam.getName() + " score is: " + secondTeamScore;
    }

    String result() {
        if (firstTeamScore == secondTeamScore) {
            return "Draw. " + firstTeam.getName() + " score is: " + firstTeamScore + ". " +
                    secondTeam.getName() + " score is: " + secondTeamScore;
        } else if (firstTeamScore > secondTeamScore) {
            return firstTeam.getName() + " is a WINNER with score: " + firstTeamScore + ". " +
                    secondTeam.getName() + " score is: " + secondTeamScore;
        } else {
            return secondTeam.getName() + " is a WINNER with score: " + secondTeamScore + ". " +
                    firstTeam.getName() + " score is: " + firstTeamScore;
        }
    }
}

class Team {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String teamName) {
        name = teamName;
    }
}