package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh04N015.getFullYears;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 27/02/17.
 */

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testGetFullYears();
    }

    private static void testGetFullYears() {
        assertEquals("TaskCh04N015Test.testGetFullYears", 29, getFullYears(12, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testGetFullYears", 28, getFullYears(5, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testGetFullYears", 29, getFullYears(6, 2014, 6, 1985));
    }
}
