package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh12N023.*;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 3/5/2017.
 */

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testFillArrA();
        testFillArrB();
        testFillArrC();
    }

    private static void testFillArrA() {
        int[][] testA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testnegativeElementsFirst", testA, fillArrA(7));
    }

    private static void testFillArrB() {
        int[][] testB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testnegativeElementsFirst", testB, fillArrB(7));
    }

    private static void testFillArrC() {
        int[][] testC = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("TaskCh12N023Test.testnegativeElementsFirst", testC, fillArrC(7));
    }
}