package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printOneDimArray;

public class TaskCh11N158 {

    public static void main(String[] args) {
        int[] input = {5, 13, 13, 13, 9, 9, 9};
//        int[] input = {54, 45, 4, 54, 54, 54, 54, 65, 876, 46, 65, 666, 237, 789, 951, 357, 258, 54, 54, 787, 987, 654, 321};
        int[] output = killTwinsElements(input.clone());
        printOneDimArray("Source array:", input);
        printOneDimArray("Modified array:", output);
    }

    public static int[] killTwinsElements(int[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = input.length - 1; j >= i + 1; j--) {
                if (input[i] == input[j]) {
                    System.arraycopy(input, j + 1, input, j, input.length - 1 - j);
                    input[input.length - 1] = 0;
                }
            }
        }
        return input;
    }
}
