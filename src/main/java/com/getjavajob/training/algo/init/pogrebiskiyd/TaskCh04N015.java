package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh04N015 {

    public static void main(String[] args) {
        int birthMonth;
        do {
            birthMonth = enterIntNumber("Enter the birth month: ");
        } while (birthMonth <= 0 || birthMonth > 12);
        int birthYear;
        do {
            birthYear = enterIntNumber("Enter the birth year: ");
        } while (birthYear <= 0);
        int currentMonth;
        do {
            currentMonth = enterIntNumber("Enter the current month: ");
        } while (currentMonth <= 0 || currentMonth > 12);
        int currentYear;
        do {
            currentYear = enterIntNumber("Enter the current year: ");
        } while (currentYear <= 0);

        System.out.println(getFullYears(currentMonth, currentYear, birthMonth, birthYear));
    }

    public static int getFullYears(int currentMonth, int currentYear, int birthMonth, int birthYear) {
        int yearDifference = currentYear - birthYear;
        return birthMonth <= currentMonth ? yearDifference : yearDifference - 1;
    }
}
