package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh09N166.swapFirstLastWords;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh09N166Test {

    public static void main(String[] args) {
        testSwapFirstLastWords();
    }

    private static void testSwapFirstLastWords() {
        assertEquals("TaskCh09N166Test.testSwapFirstLastWords", "worms the comic storms i am the tiny iam",
                swapFirstLastWords("iam the comic storms i am the tiny worms"));
        assertEquals("TaskCh09N166Test.testSwapFirstLastWords", "Expression length should be not more than 10 words.",
                swapFirstLastWords("i am the comic storms i am the tiny worms"));
    }
}
