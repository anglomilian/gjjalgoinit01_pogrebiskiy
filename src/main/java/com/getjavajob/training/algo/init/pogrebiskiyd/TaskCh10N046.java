package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.enterIntNumber;

public class TaskCh10N046 {

    public static void main(String[] args) {
        int firstMember = enterIntNumber("Enter first member of geometic progression: ");
        int progressionDiff = enterIntNumber("Enter difference of geometic progression: ");
        int quantity = enterIntNumber("Enter quantity of the members: ");
        System.out.println(nMemberOfGeometricProgression(firstMember, progressionDiff, quantity));
        System.out.println(sumNNumbersOfGeometicProgression(firstMember, progressionDiff, quantity));
    }

    public static int nMemberOfGeometricProgression(int firstMember, int progressionDiff, int quantity) {
        if (quantity == 1) {
            return firstMember;
        }
        return progressionDiff * nMemberOfGeometricProgression(firstMember, progressionDiff, quantity - 1);
    }

    public static int sumNNumbersOfGeometicProgression(int firstMember, int progressionDiff, int quantity) {
        if (quantity == 0) {
            return 0;
        }
        return firstMember + sumNNumbersOfGeometicProgression(firstMember * progressionDiff, progressionDiff, quantity - 1);
    }
}
