package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printOneDimArray;

public class TaskCh11N245 {

    public static void main(String[] args) {
        int[] input = {8, 54, 7, 15, -8, -5, 69, 78, 42, 765, 23, -9, 765, -4, -67, 58, 89, 456, 123, -87};
        printOneDimArray("Input array:", input);
        printOneDimArray("Output array:", negativeElementsFirst(input));
    }

    public static int[] negativeElementsFirst(int[] input) {
        int[] output = new int[input.length];
        int j = 0;
        int k = input.length - 1;
        for (int anInput : input) {
            if (anInput < 0) {
                output[j] = anInput;
                j++;
            } else {
                output[k] = anInput;
                k--;
            }
        }
        return output;
    }
}
