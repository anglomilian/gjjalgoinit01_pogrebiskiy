package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.util.InputData.printTwoDimArray;

public class TaskCh12N028 {

    public static void main(String[] args) {
        int arraySize = 9; //arraySize % 2 = 1;
        int[][] output;
        output = arraySnake(arraySize);
        printTwoDimArray("Snake", output);
    }

    public static int[][] arraySnake(int arraySize) {
        int j;
        int z = 1;
        int restrictor1 = 0;
        int restrictor2 = 0;
        int[][] output = new int[arraySize][arraySize];
        int loopSteps = output.length / 2 + 1;
        while (loopSteps - restrictor1 > 0) {
            int i = restrictor1;
            for (j = restrictor1++; j < output.length - restrictor2; j++) {
                output[i][j] = z++;
            }
            j = output.length - 1 - restrictor2;
            for (i = restrictor1; i < output.length - restrictor2; i++) {
                output[i][j] = z++;
            }
            i = output.length - 1 - restrictor2;
            for (j = output.length - 1 - restrictor1; j >= restrictor2; j--) {
                output[i][j] = z++;
            }
            j = restrictor2++;
            for (i = output.length - 1 - restrictor2; i >= restrictor1; i--) {
                output[i][j] = z++;
            }
        }
        return output;
    }
}