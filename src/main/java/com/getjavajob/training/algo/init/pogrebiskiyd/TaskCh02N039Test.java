package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh02N039.getHoursArrowDegree;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by @author anglomilian on 2/26/2017.
 */
public class TaskCh02N039Test {
    public static void main(String[] args) {
        testGetHoursArrowDegree();
    }

    private static void testGetHoursArrowDegree() {
        assertEquals("TaskCh02N039Test.testGetHoursArrowDegree", 90.0, getHoursArrowDegree(3, 0, 0));
        assertEquals("TaskCh02N039Test.testGetHoursArrowDegree", 292.5, getHoursArrowDegree(21, 45, 50));
    }
}
