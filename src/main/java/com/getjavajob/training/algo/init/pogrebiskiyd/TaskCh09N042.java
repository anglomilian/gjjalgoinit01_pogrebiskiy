package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;


import static main.java.com.getjavajob.training.util.InputData.enterString;

public class TaskCh09N042 {

    public static void main(String[] args) {
        String oneWord;
        do {
            oneWord = enterString("Enter one word: ");
        } while (oneWord.contains(" "));
        System.out.println(getReverseWord(oneWord));
    }

    public static String getReverseWord(String oneWord) {
        StringBuilder sb = new StringBuilder(oneWord);
        return sb.reverse().toString();
    }
}
