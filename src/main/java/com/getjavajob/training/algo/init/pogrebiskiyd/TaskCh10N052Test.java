package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh10N052.reverseNumber;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 29/03/17.
 */

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverseNumber();
    }

    private static void testReverseNumber() {
        assertEquals("TaskCh10N052Test.testReverseNumber", 654321, reverseNumber(123456));
    }
}
