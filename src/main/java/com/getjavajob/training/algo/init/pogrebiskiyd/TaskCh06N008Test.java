package main.java.com.getjavajob.training.algo.init.pogrebiskiyd;

import static main.java.com.getjavajob.training.algo.init.pogrebiskiyd.TaskCh06N008.getSquareRow;
import static main.java.com.getjavajob.training.util.Assert.assertEquals;

/**
 * Created by denis on 28/02/17.
 */

public class TaskCh06N008Test {

    public static void main(String[] args) {
        testGetSquareRow();
    }

    private static void testGetSquareRow() {
        int[] testRow = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484, 529};
        assertEquals("TaskCh06N008Test.testGetSqaureRow", testRow, getSquareRow(570));
        int[] testRow1 = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256};
        assertEquals("TaskCh06N008Test.testGetSqaureRow", testRow1, getSquareRow(256));
    }
}
